import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddProductComponent } from './components/add-product/add-product.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { EditProductComponent } from './components/edit-product/edit-product.component';
import { CartViewComponent } from './components/cart-view/cart-view.component';
import { AuthGuard } from './shared/auth.guard';
import { LoginComponent } from './components/login/login.component';
import { ProductCrudComponent } from './components/product-crud/product-crud.component';
import { ViewProductComponent } from './components/view-product/view-product.component';


const routes: Routes = [
{
  path:'',
  component: LoginComponent
},
{
  path:'login',
  component: LoginComponent
},
{
  path: 'productlist', canActivate: [AuthGuard] ,
  component: ProductListComponent
},
{
  path:'view-product/:id',
  component: ViewProductComponent
},
{
  path:'edit-product/:id',
  component: EditProductComponent
},
{
  path: 'addproduct',
  component: AddProductComponent
},
{
  path: 'productcrud',
  component: ProductCrudComponent
},
{
  path: 'cart', canActivate: [AuthGuard] ,
  component: CartViewComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
