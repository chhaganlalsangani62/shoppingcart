export class Product {
    id: string;
    product_name: string;
    description: string;
    price: number;
    quantity: number;
    category: string;
    image: string
 }
