import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddProductComponent } from './components/add-product/add-product.component';
import { EditProductComponent } from './components/edit-product/edit-product.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { HttpClientModule } from '@angular/common/http';
import { ProductService } from './shared/product.service';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CartViewComponent } from './components/cart-view/cart-view.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './shared/auth.guard';
import { CommonModule } from "@angular/common";
import { ProductCrudComponent } from './components/product-crud/product-crud.component';
import { ViewProductComponent } from './components/view-product/view-product.component';




@NgModule({
  declarations: [
    AppComponent,
    AddProductComponent,
    EditProductComponent,
    ProductListComponent,
    CartViewComponent,
    LoginComponent,
    ProductCrudComponent,
    ViewProductComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [AuthGuard,ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
