import { Component, OnInit, NgZone } from '@angular/core';
import { ProductService } from '../../shared/product.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  ProductsList: any = [];
  updateProductForm: FormGroup;

  ngOnInit() {
    this.updateForm()
  }

  constructor(
    private actRoute: ActivatedRoute,
    public productService: ProductService,
    public fb: FormBuilder,
    private ngZone: NgZone,
    private router: Router
  ) {
    var id = this.actRoute.snapshot.paramMap.get('id');
    this.productService.GetProduct(id).subscribe((data) => {
      this.updateProductForm = this.fb.group({
        product_name: [data.product_name],
        description: [data.description],
        price: [data.price],
        quantity: [data.quantity],
        category: [data.category]
      })
    })
  }

  updateForm() {
    this.updateProductForm = this.fb.group({
      product_name: [''],
      description: [''],
      price: [],
      quantity: [],
      category: ['']
    })
  }

  submitForm() {
    var id = this.actRoute.snapshot.paramMap.get('id');
    this.productService.UpdateProduct(id, this.updateProductForm.value).subscribe(res => {
      this.ngZone.run(() => this.router.navigateByUrl('/productcrud'))
    })
  }



}
