import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { MyserviceService } from '../../shared/myservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [MyserviceService]
})
export class LoginComponent implements OnInit {
  msg;
  constructor(private service: MyserviceService, private routes: Router) { }

  ngOnInit() {
  }
  check(uname: string, p: string) {
    var output = this.service.checkusernameandpassword(uname, p);
    if (output == true) {
      this.routes.navigate(['/productlist']);
    }
    else {
      this.msg = 'Invalid username or password';
    }
  }


}
