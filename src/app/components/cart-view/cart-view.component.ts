import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../shared/product.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cart-view',
  templateUrl: './cart-view.component.html',
  styleUrls: ['./cart-view.component.css']
})
export class CartViewComponent implements OnInit {

  ProductsList: any = [];
  imgURL = '../../../assets/images/image1.png';
  totalPrice = 0;

  constructor(public productService: ProductService,
    private actRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.loadProducts();
  }

  loadProducts() {
    return this.productService.GetCartProducts().subscribe((data: {}) => {
      this.ProductsList = data;
      for (let index = 0; index < this.ProductsList.length; index++) {
        this.totalPrice = this.totalPrice + Number(this.ProductsList[index].price);
      }
    });
  }

  deleteProduct(data) {
    var index = index = this.ProductsList.map(x => { return x.product_name }).indexOf(data.product_name);
    return this.productService.DeleteCartProduct(data.id).subscribe(res => {
      this.ProductsList.splice(index, 1);
      console.log('Product deleted from cart!');
      this.totalPrice = 0;
      for (let index = 0; index < this.ProductsList.length; index++) {
        this.totalPrice = this.totalPrice + Number(this.ProductsList[index].price);
      }
    })
  }
}
