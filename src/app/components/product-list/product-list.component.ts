import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../shared/product.service';
import { Product } from 'src/app/shared/product';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  ProductsList: any = [];
  imgURL = '../../../assets/images/image1.png';
  product: Product;


  ngOnInit() {
    this.loadProducts();
  }

  constructor(
    public productService: ProductService
  ) { }

  loadProducts() {
    return this.productService.GetProducts().subscribe((data: {}) => {
      this.ProductsList = data;
    })
  }


  addToCart(data, event) {
    if (event.srcElement.innerHTML === 'Add To Cart') {
      event.srcElement.innerHTML = "Remove From Cart";
      this.product = data;
      this.product.quantity = 1;
      this.productService.AddToCart(this.product).subscribe(res => {
        console.log('Product added!')
      });
    }
    else {
      event.srcElement.innerHTML = "Add To Cart";

      return this.productService.DeleteCartProduct(data.id).subscribe(res => {

        console.log('Product deleted!')
      })
    }

  }





}
