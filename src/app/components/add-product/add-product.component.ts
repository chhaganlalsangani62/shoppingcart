import { Component, OnInit, NgZone } from '@angular/core';
import { ProductService } from '../../shared/product.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  productForm: FormGroup;
  ProductArr: any = [];
  submitted = false;
  fileData: File = null;
  previewUrl: any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;

  ngOnInit() {
    this.addProduct();
  }

  constructor(
    public fb: FormBuilder,
    private ngZone: NgZone,
    private router: Router,
    public productService: ProductService
  ) { }

  addProduct() {
    this.productForm = this.fb.group({
      product_name: ['', Validators.required],
      description: ['', Validators.required],
      price: [, Validators.required],
      quantity: [, Validators.required],
      category: ['', Validators.required],
      image: ['../../../assets/images/image1.png', Validators.required]
    })
  }
  get fval() {
    return this.productForm.controls;
  }
  submitForm() {
    this.submitted = true;
    if (this.productForm.invalid) {
      return;
    }

    this.productService.CreateProduct(this.productForm.value).subscribe(res => {
      console.log('Product added!')
      this.ngZone.run(() => this.router.navigateByUrl('/productcrud'))
    });
  }

  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
    this.preview();

  }

  preview() {
    // Show preview 
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
    }
  }


}
