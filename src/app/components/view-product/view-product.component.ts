import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../shared/product.service';
import { Product } from 'src/app/shared/product';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.css']
})
export class ViewProductComponent implements OnInit {

  Product: any;
  imgURL = '../../../assets/images/image1.png';
  product: Product;


  ngOnInit() {
    this.loadProducts();
  }

  constructor(
    public productService: ProductService, private actRoute: ActivatedRoute,
  ) { }

  loadProducts() {
    var id = this.actRoute.snapshot.paramMap.get('id');
    return this.productService.GetProduct(id).subscribe((data: {}) => {
      this.Product = data;
    })
  }


}
