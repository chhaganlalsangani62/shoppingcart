import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../shared/product.service';
import { Product } from 'src/app/shared/product';

@Component({
  selector: 'app-product-crud',
  templateUrl: './product-crud.component.html',
  styleUrls: ['./product-crud.component.css']
})
export class ProductCrudComponent implements OnInit {
  ProductsList: any = [];
  imgURL = '../../../assets/images/image1.png';
  product: Product;


  ngOnInit() {
    this.loadProducts();
  }

  constructor(
    public productService: ProductService
  ) { }

  loadProducts() {
    return this.productService.GetProducts().subscribe((data: {}) => {
      this.ProductsList = data;
    })
  }

  deleteProduct(data) {
    var index = index = this.ProductsList.map(x => { return x.product_name }).indexOf(data.product_name);
    return this.productService.DeleteProduct(data.id).subscribe(res => {
      this.ProductsList.splice(index, 1)
      console.log('Product deleted!')
    })
  }

}
